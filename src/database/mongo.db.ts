import mongoose from 'mongoose';

export async function connectToMongoDatabase() {
  console.log("Connecting to database...")
  const {
    MONGO_USER,
    MONGO_PASSWORD,
    MONGO_PATH,
    MONGO_DATABASE,
    MONGO_PORT
  } = process.env;
  try {
    await mongoose.connect(`mongodb://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_PATH}:${MONGO_PORT}/${MONGO_DATABASE}?authSource=admin`,
      { useNewUrlParser: true, useUnifiedTopology: true, connectTimeoutMS: 10 }
    )
    console.log("Connected to database successfully!")
  } catch (e) {
    console.log("MongoDB couldn't connect! Trying again...(5s)");
    await new Promise(resolve => setTimeout(resolve, 5000));
    await connectToMongoDatabase()
  }
}