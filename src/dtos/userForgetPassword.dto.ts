import { IsArray, IsString } from 'class-validator';

class UserForgetPasswordDto {
  @IsArray()
  public seedPhrase: string[];

  @IsString()
  public newPassword: string;
}

export default UserForgetPasswordDto;