import { IsString } from 'class-validator';

class UserLogInDto {
  @IsString()
  public username: string;

  @IsString()
  public password: string;
}

export default UserLogInDto;
