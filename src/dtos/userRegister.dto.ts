import { IsString, Length } from 'class-validator';

class UserRegisterDto {
  @IsString()
  public username: string;

  @IsString()
  public password: string;
}

export default UserRegisterDto;
