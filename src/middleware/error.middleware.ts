import { NextFunction, Request, Response } from 'express';
import ResponseBuilder from '../utils/helpers/responseBuilder';
import HttpException from '../exceptions/HttpException';

function errorMiddleware(error: HttpException, request: Request, response: Response, next: NextFunction) {
  const status = error.status || 500;
  const message = error.message || 'Something went wrong';
  response
    .status(status)
    .send(new ResponseBuilder({
      success: false, 
      message
    }));
}

export default errorMiddleware;
