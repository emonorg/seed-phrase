import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import Md5 from 'md5';
import RandomWords from 'random-words';
import UserRegisterDto from '../dtos/userRegister.dto';
import UserForgetPasswordDto from '../dtos/userForgetPassword.dto';
import UserLogInDto from '../dtos/userLogIn.dto';
import DataStoredInToken from '../interfaces/dataStoredInToken';
import TokenData from '../interfaces/tokenData.interface';
import User from '../interfaces/user.interface';
import UserModel from '../models/user.model';
import HttpException from '../exceptions/HttpException';

export default class UserService {
  public async register(userRegisterDto: UserRegisterDto) {
    try {
      if (
        await UserModel.findOne({ username: userRegisterDto.username })
      ) {
        throw new HttpException(400, 'Username has been taken before!');
      }
      const hashedPassword = await bcrypt.hash(userRegisterDto.password, 10);
      const seedPhrase = RandomWords(12);
      const user = await UserModel.create({
        ...userRegisterDto,
        password: hashedPassword,
        seedHash: await this.hashSeedPhrase(seedPhrase),
      });
      user.password = undefined
      user.seedHash = undefined
      const tokenData = this.createToken(user);
      return {
        tokenData,
        user,
        seedPhrase,
      };
    } catch (err) {
      throw err;
    }
  }

  public async login(userLoginDto: UserLogInDto): Promise<TokenData> {
    try {
      const user = await UserModel.findOne({ username: userLoginDto.username });
      if (user) {
        const isPasswordMatching = await bcrypt.compare(
          userLoginDto.password,
          user.get('password', null, { getters: false }),
        );
        if (!isPasswordMatching) throw new HttpException(401, 'Access denied!')
        const tokenData = this.createToken(user);
        return tokenData
      }
    } catch (err) {
      throw err;
    }
  }

  public async forgetPassword(userForgetPasswordDto: UserForgetPasswordDto): Promise<void> {
    try {
      let hashedSeed = await this.hashSeedPhrase(userForgetPasswordDto.seedPhrase)
      let user = await UserModel.updateOne({ seedHash: hashedSeed }, { password:  await bcrypt.hash(userForgetPasswordDto.newPassword, 10) })
      if (user.nModified === 0) throw new HttpException(401, 'Access denied!');
    } catch (err) {
      throw err;
    }
  }

  public createToken(user: User): TokenData {
    const expiresIn = 60 * 60; // an hour
    const secret = process.env.JWT_SECRET;
    const dataStoredInToken: DataStoredInToken = {
      _id: user._id,
    };
    return {
      expiresIn,
      token: jwt.sign(dataStoredInToken, secret, { expiresIn }),
    };
  }

  private async hashSeedPhrase(seedPhrase: string[]): Promise<string> {
    let seedString = '';
    for (let phrase of seedPhrase) {
      seedString += phrase;
    }
    let hashed = Md5(seedString);
    return hashed;
  }
}