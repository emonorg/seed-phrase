interface User {
  _id: string;
  username: string;
  password: string;
  seedHash: string;
}

export default User;