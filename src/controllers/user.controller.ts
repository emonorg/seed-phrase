import { NextFunction, Router, Request, Response } from 'express';
import UserService from '../services/user.service';
import Controller from '../interfaces/controller.interface';
import TokenData from '../interfaces/tokenData.interface';
import validationMiddleware from '../middleware/validation.middleware';
import UserRegisterDto from '../dtos/userRegister.dto';
import UserLogInDto from '../dtos/userLogIn.dto';
import ResponseBuilder from '../utils/helpers/responseBuilder';
import UserForgetPasswordDto from '../dtos/userForgetPassword.dto';

export default class AuthenticationController implements Controller {
  public path = '/auth';
  public router = Router();
  public userService = new UserService();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.post(`${this.path}/register`, validationMiddleware(UserRegisterDto), this.registration);
    this.router.post(`${this.path}/login`, validationMiddleware(UserLogInDto), this.login);
    this.router.post(`${this.path}/recovery`, validationMiddleware(UserForgetPasswordDto), this.forgetPassword);
  }

  private registration = async (req: Request, res: Response, next: NextFunction) => {
    const userRegisterDto: UserRegisterDto = req.body;
    try {
      let result = await this.userService.register(userRegisterDto);
      return res.send(new ResponseBuilder({
        success: true,
        message: 'User signed up successfully!',
      }, result));
    } catch (error) {
      next(error);
    }
  }

  private login = async (req: Request, res: Response, next: NextFunction) => {
    const userLoginDto: UserLogInDto = req.body;
    try {
      let result: TokenData = await this.userService.login(userLoginDto);
      return res.send(new ResponseBuilder({
        success: true,
        message: 'User logged in successfully!',
      }, result));
    } catch (error) {
      next(error);
    }
  }

  private forgetPassword = async (req: Request, res: Response, next: NextFunction) => {
    const userForgetPassword: UserForgetPasswordDto = req.body;
    try {
      await this.userService.forgetPassword(userForgetPassword);
      return res.send(new ResponseBuilder({
        success: true,
        message: 'New password has been committed successfully!',
      }));
    } catch (error) {
      next(error);
    }
  }
}