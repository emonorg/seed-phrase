import {
  cleanEnv, port, str,
} from 'envalid';

function validateEnv() {
  cleanEnv(process.env, {
    JWT_SECRET: str(),
    MONGO_PASSWORD: str(),
    MONGO_PATH: str(),
    MONGO_USER: str(),
    MONGO_DATABASE: str(),
    MONGO_PORT: str(),
    PORT: port(),
  });
}

export default validateEnv;
