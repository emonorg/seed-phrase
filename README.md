# Seed phrase mini project

### Deployment
Simply use `docker-compose up` for testing the project!

> If you prefer changing deployment configuration, take a look at environment section of docker-compose.yml

## NOTES
1. I always write tests for my project; due to time limitations (given that I'm working in a company), I haven't included any tests for this project, sorry! :(
2. I'm a fan of CLEAN CODE, so all of the logic is clear, it doesn't need any code comments.
3. I have added a postman collection, if you want to check the endpoints.